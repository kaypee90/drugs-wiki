# Drug search wiki


## Prerequisites

To run you will need docker installed.

### Commands

To be able to run you will need docker since it is a dockerized app.


 * Build container: `docker-compose build`
 * Run application: `docker-compose up`
 * Stop application: `docker-compose down`

## URLS

Frontend Application  **GET** `http://localhost:8000/app/`

Search Endpoint  **GET** `http://localhost:8000/drugs/search/?q={search_keywords}`


_For more on endpoint documentation, please refer to the [Swagger Docs](http://localhost:8000/swagger/)_

<p align="right">(<a href="#top">back to top</a>)</p>

## Built With

* [django](https://www.djangoproject.com/) - High-level Python web framework
* [django restframework](https://www.django-rest-framework.org/) - Powerful and flexible toolkit for building Web APIs
* [elasticsearch](https://www.elastic.co/) - Fast and scalable search and analytics engine 
* [docker](https://www.docker.com/) - Containerization technology


## Authors

* **Kwabena** - *Developer*