import json
from rest_framework import serializers

from drf_yasg.utils import swagger_serializer_method


class StringListField(serializers.ListField):
    child = serializers.CharField()


class DrugSearchSerializer(serializers.Serializer):
    name = serializers.CharField()
    diseases = serializers.SerializerMethodField()
    released = serializers.DateField(format="%d/%m/%Y")
    description = serializers.CharField()

    @swagger_serializer_method(serializer_or_field=StringListField)
    def get_diseases(self, obj):
        return json.loads(obj.diseases)
