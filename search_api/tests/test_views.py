from search_api.models import Drug
from django.test import TestCase
from haystack.query import SearchQuerySet


class SearchViewTests(TestCase):
    fixtures = ["seed.json"]

    def test_search(self):
        results = SearchQuerySet().all()
        assert results.count() == Drug.objects.count()

    def test_search_with_valid_keyword(self):
        response = self.client.get("/drugs/search/?q=Folic+Acid")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["count"], 1)
        self.assertEqual(response.json()["results"][0]["name"], "Folic Acid")

    def test_search_with_non_matching_keyword(self):
        response = self.client.get("/drugs/search/?q=zakari")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["count"], 0)

    def test_search_with_empty_keyword(self):
        response = self.client.get("/drugs/search/?")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["count"], 0)
