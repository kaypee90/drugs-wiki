import json
from haystack import indexes
from search_api.models import Drug


class DrugIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Builds an index for drug model
    """

    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr="name")
    diseases = indexes.CharField(model_attr="diseases")
    released = indexes.DateField(model_attr="released")
    description = indexes.CharField(model_attr="description")
    content_auto = indexes.EdgeNgramField(use_template=True)

    def get_model(self):
        return Drug

    def prepare_diseases(self, obj):
        return json.dumps(obj.diseases)

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
