import uuid
from django.db import models


class Drug(models.Model):
    """
    Persists drug details
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=64)
    description = models.TextField()
    released = models.DateField()
    diseases = models.JSONField(default=[])
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
