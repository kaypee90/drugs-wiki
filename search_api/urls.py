from django.contrib import admin
from django.urls import path
from search_api.views import DrugSearchViewSet

urlpatterns = [
    path("drugs/search/", DrugSearchViewSet.as_view({"get": "list"})),
]
