import logging
from django.db.models import Q
from rest_framework import mixins, viewsets
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from haystack.query import SearchQuerySet
from search_api.serializers import DrugSearchSerializer

logger = logging.getLogger()

search_keywords_param = openapi.Parameter(
    "q",
    openapi.IN_QUERY,
    description="Search by drug name or disease",
    type=openapi.TYPE_STRING,
)


class DrugSearchViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    Viewset for drug search endpoint
    """
    serializer_class = DrugSearchSerializer

    def get_queryset(self, *args, **kwargs):
        params = self.request.query_params
        queryset = SearchQuerySet().none()
        keywords = params.get("q")
        if keywords:
            queryset = SearchQuerySet().autocomplete(content_auto=keywords)
        else:
            logger.warning("No search keyword was provided")

        return queryset

    @swagger_auto_schema(manual_parameters=[search_keywords_param])
    def list(self, request):
        """
        Get search results
        """
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
