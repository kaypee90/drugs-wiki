from django.shortcuts import render


def index(request):
    """
    Render frontend search page
    """
    return render(request, "search.html")
