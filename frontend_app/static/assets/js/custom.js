function showPagination(previous, next) {
	$(".pgPrevious").attr('onclick', `return searchDrugs('${previous}');`)
	$(".pgNext").attr('onclick', `return searchDrugs('${next}');`)
	
	if (previous != null){
		$(".pgPrevious").show()
	}
	else {
		$(".pgPrevious").hide();
	}

	if (next != null){
		$(".pgNext").show()
	}
	else {
		$(".pgNext").hide();
	}

}

function searchDrugs(url) {
	$.ajax({
		type: 'GET',
		crossDomain: true,
		dataType: 'json',
		url: url,
		success: function(jsondata){
			var searchResult = jsondata.results;
			showPagination(jsondata.previous, jsondata.next)
			var searchResultsCount = `
					<div class="row">
						<div class="col-md-12 text-center p-2">
							<span> showing ${searchResult.length} of ${jsondata.count} results </span>
							<span class="ml-6"><a href="/app/">clear</a></span>
						</div>
					</div>
				`		
				var results = [searchResultsCount];
		
				searchResult.forEach(function (item, _) {
					var drugDetails = `
						<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-body">
								<div class="row mb-6">
									<div class="col-md-6"> <h5 class="card-title">${item.name}</h5></div>
									<div class="col-md-6 text-right"><span>Released Date: ${item.released}</span></div>
								</div>
									<div class="p-4">
									<h6 class="card-subtitle mb-2 text-muted">Related Diseases: </h6>
									<ul>
										<li> - ${item.diseases[0]}</li>
										<li> - ${item.diseases[1]}</li>
										<li> - ${item.diseases[2]}</li>
									</ul>
									</div>
									<div class="expanel-footer">
										<p class="card-text">${item.description}</p>
									</div>
								
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
						`;
						results.push(drugDetails);
				});
				
			$("#btnSearch").removeClass("btn-loading");
			$(".search-results").html(results.join(''));
		},
		error: function(e){
			$("#btnSearch").removeClass("btn-loading");
			console.error(e)
		}
	 });	

	 return true;
}


(function($) {
    "use strict";

	// ______________ Page Loading
	$(window).on("load", function(e) {
		$("#global-loader").fadeOut("slow");
	})

	var baseUrl = "http://localhost:8000/drugs/search?q=";


	$(document).ready(function(){
		$("#btnSearch").click(function(){
			var searchKeyword = $("#txtSearch").val();
			var url = baseUrl + searchKeyword;
			$("#btnSearch").addClass("btn-loading");

			searchDrugs(url);
		});
	});
		
})(jQuery);

